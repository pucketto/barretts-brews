﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BarrettsBrews.Models
{
    public class Beer : ObservableObject
    {
        private string beerId = string.Empty;
        public string BeerId
        {
            get { return beerId; }
            set { SetProperty(ref beerId, value); }
        }

        private string brewingCompany = string.Empty;
        public string BrewingCompany
        {
            get { return brewingCompany; }
            set { SetProperty(ref brewingCompany, value); }
        }

        private List<Rating> ratings;
        public List<Rating> Ratings
        {
            get { return ratings; }
            set { SetProperty(ref ratings, value); }
        }
    }
}

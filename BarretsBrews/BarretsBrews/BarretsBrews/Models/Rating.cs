﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BarrettsBrews.Models
{
    public class Rating : ObservableObject
    {
        private string ratingId = string.Empty;
        public string RatingId
        {
            get { return ratingId; }
            set { SetProperty(ref ratingId, value); }
        }

        private string beerId = string.Empty;
        public string BeerId
        {
            get { return beerId; }
            set { SetProperty(ref beerId, value); }
        }

        private int ratingValue;
        public int RatingValue
        {
            get { return ratingValue; }
            set { SetProperty(ref ratingValue, value); }
        }

        private string comment;
        public string Comment
        {
            get { return comment; }
            set { SetProperty(ref comment, value); }
        }
    }
}

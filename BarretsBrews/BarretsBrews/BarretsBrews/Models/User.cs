﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BarrettsBrews.Models
{
    public class User : ObservableObject
    {
        private string userId = string.Empty;
        public string UserId
        {
            get { return userId; }
            set { SetProperty(ref userId, value); }
        }

        private string password = string.Empty;
        public string Password
        {
            get { return password; }
            set { SetProperty(ref password, value); }
        }

        private string userAccountName = string.Empty;
        public string UserAccountName
        {
            get { return userAccountName; }
            set { SetProperty(ref userAccountName, value); }
        }

        private string userActualName = string.Empty;
        public string UserActualName
        {
            get { return userActualName; }
            set { SetProperty(ref userActualName, value); }
        }

        private string userEmail = string.Empty;
        public string UserEmail
        {
            get { return userEmail; }
            set { SetProperty(ref userEmail, value); }
        }

        private int age;
        public int Age
        {
            get { return age; }
            set { SetProperty(ref age, value); }
        }

        private byte[] picture;
        public byte[] Picture
        {
            get { return picture; }
            set { SetProperty(ref picture, value); }
        }

        private List<User> friends;
        public List<User> Friends
        {
            get { return friends; }
            set { SetProperty(ref friends, value); }
        }

        private List<Beer> ratedBeers;
        public List<Beer> RatedBeers
        {
            get { return ratedBeers; }
            set { SetProperty(ref ratedBeers, value); }
        }
    }

}

﻿using BarrettsBrews.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BarrettsBrews.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BeerPage : ContentPage
	{
        BeerViewModel viewModel;
		public BeerPage ()
		{
			InitializeComponent ();
            BindingContext = viewModel = new BeerViewModel();
		}
	}
}